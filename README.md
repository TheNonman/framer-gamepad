# Framer Gamepad
Framer Gamepad will contain 2 modules, one for Framer and one for Framer X, that will make it easy to connect and begin creating UI's with support for Gamepad input.

This project is just starting up. Currently, the Framer module is usable as it was forked from Emil Widlund's project  [Framer Joystick](https://github.com/emilwidlund/framer-joystick) as noted in the History/Background section below. The Framer X version has not been tested yet as of this writing and it is not expected to work until it has been converted to work as a React component structure.

## Computer Gamepad Use Notes

These modules leverage the Web Gamepad APIs which have pretty strong support these days. Of course, for the Gamepad APIs to work, your computer must be able to recognize and communicate with your controller on a hardware level first.

Here we will focus on 2 common Gamepad controllers, Xbox controllers (specifically, standard 360 and One controllers) and PlayStation controllers (specifically, standard PlayStation 3 and 4 controllers). In both cases, we will assume connection via a USB cable.

Windows 10 (as of this writing) can use the gamepads listed above with plug and play driver installation.

MacOS (10.13+) can use the listed PlayStation controllers without additional software. However, to use Xbox controllers additional drivers are required. This author uses and recommends [360Controller](https://github.com/360Controller/360Controller).

## Testing Web API Gamepad Use

A great resource for testing your Gamepads, specifically as they work with the Web Gamepad APIs, is the [Gamepad Viewer website](https://gamepadviewer.com). If you're controller is already plugged in when you access the site, be sure to press a button while the browser has focus to get an active gamepad connection.

## History/Background
The Framer (CoffeScript) version was originally based on the project, [Framer Joystick by Emil Widlund](https://github.com/emilwidlund/framer-joystick). Along with the release of "Framer Joystick," Emil wrote this article on using his project: [Introducing Framer Joystick](https://blog.framer.com/introducing-framer-joystick-28359287bef0)
