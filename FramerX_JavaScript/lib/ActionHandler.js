// Generated by CoffeeScript 1.12.7
(function() {
  var Broadcaster, Gamepad, _;

  Gamepad = require('./Gamepad.coffee').Gamepad;

  Broadcaster = require('./Broadcaster.coffee').Broadcaster;

  _ = Framer._;

  exports.ActionHandler = (function() {
    function ActionHandler() {
      this.focusableActions = [];
      this.viewActions = [];
      Gamepad.on('gamepadevent', (function(_this) {
        return function(event) {
          var action;
          if (event.keyCode < 4) {
            action = _.find(_this.actions(), {
              keyCode: event.keyCode
            });
            if (action) {
              return action["function"]();
            }
          }
        };
      })(this));
      window.addEventListener('keydown', (function(_this) {
        return function(event) {
          var action;
          if (event.keyCode === 13) {
            action = _.find(_this.actions(), {
              keyCode: 0
            });
            if (action) {
              action["function"]();
            }
          }
          if (event.keyCode === 8) {
            action = _.find(_this.actions(), {
              keyCode: 1
            });
            if (action) {
              return action["function"]();
            }
          }
        };
      })(this));
      Broadcaster.on('focusEvent', (function(_this) {
        return function(focusable) {
          return _this.focusableActions = focusable.actions;
        };
      })(this));
    }

    ActionHandler.prototype.actions = function() {
      return this.focusableActions.concat(this.viewActions);
    };

    ActionHandler.prototype.clearActions = function() {
      this.focusableActions = [];
      return this.viewActions = [];
    };

    return ActionHandler;

  })();

}).call(this);
